package net.scottec.oosp5.gui.init;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import net.scottec.oosp5.Main;

public class InitController {

    private Main main;

    @FXML
    private Button btYes;

    @FXML
    private Button btNo;

    public void setMain(Main _main) {
        this.main = _main;
    }

    @FXML
    public void onYesClick(Event event) {
        ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
        this.main.init(true);
}

    @FXML
    public void onNoClick(Event event) {
        ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
        this.main.init(false);
    }
}
