package net.scottec.oosp5.gui.login;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import net.scottec.oosp5.Main;
import net.scottec.oosp5.benutzer.Benutzer;

public class LoginController {

    private Main main;
    private boolean neuAnmeldung = false;

    @FXML
    private TextField txtUserId;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private CheckBox cbNeuAnmeldung;

    @FXML
    private Button btSubmit;

    @FXML
    private Label lbError;


    public void setMain(Main _main) {
        this.main = _main;
    }

    @FXML
    public void onSubmitClick(Event event) {
        System.out.println("[LoginController] Ausführen:");
        this.lbError.setVisible(false);
        if (this.neuAnmeldung) {
            ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
            this.main.neuAnmeldung();
        }
        else {
            Benutzer benutzer = new Benutzer(this.txtUserId.getText(), this.txtPassword.getText().toCharArray());
//            ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
            this.main.benutzerLogin(benutzer);
        }
    }

    @FXML
    public void onNeuAnmeldungToggle(Event event) {
        this.neuAnmeldung = cbNeuAnmeldung.isSelected();
        System.out.println("[LoginController] Neu-Anmeldung : " + this.neuAnmeldung) ;
    }

    public void showError(String... _errorMessage) {
        if (_errorMessage.length != 0)
            this.lbError.setText(_errorMessage[0]);
        this.lbError.setVisible(true);
    }
}
