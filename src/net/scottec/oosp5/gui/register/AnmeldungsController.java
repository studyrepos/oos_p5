package net.scottec.oosp5.gui.register;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import net.scottec.oosp5.Main;
import net.scottec.oosp5.benutzer.Benutzer;

public class AnmeldungsController {

    private Main main;

    @FXML
    TextField txtUserId;

    @FXML
    PasswordField txtPassword;

    @FXML
    PasswordField txtPasswordRepeat;

    @FXML
    Label lbError;

    @FXML
    Button btSubmit;

    public void setMain(Main _main) {
        this.main = _main;
    }

    @FXML
    public void onSubmitClick(Event event) {
        this.lbError.setVisible(false);
        if(txtPassword.getText().equals(txtPasswordRepeat.getText())) {
            lbError.setText("");
            Benutzer benutzer = new Benutzer(txtUserId.getText(), txtPassword.getText().toCharArray());
            this.main.neuerBenutzer(benutzer);
        }
        else {
            System.out.println("Error");
            showError("Passwort != Wiederholung");
        }
    }

    public void showError(String... _errorMessage) {
        if (_errorMessage.length != 0)
            this.lbError.setText(_errorMessage[0]);
        this.lbError.setVisible(true);
    }
}
