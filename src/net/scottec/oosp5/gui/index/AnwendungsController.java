package net.scottec.oosp5.gui.index;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import net.scottec.oosp5.Main;

public class AnwendungsController {

    private Main main;

    @FXML
    Label lbMessage;

    @FXML
    Button btCancel;

    public void setMain(Main _main) {
        this.main = _main;
    }

    @FXML
    public void onCancelClick(Event event) {
        System.out.println("[AnwendungsController] Event : Abbrechen");
        ((Stage)((Node) event.getSource()).getScene().getWindow()).close();
    }
}
