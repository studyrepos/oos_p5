package net.scottec.oosp5.benutzer;

/**
 * Exception
 * Kennzeichnet Dopplung in Datenhaltung
 */
public class BenutzerDoppeltException extends Exception {
    public BenutzerDoppeltException(String message) {
        super(message);
    }
}
